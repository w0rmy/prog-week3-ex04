﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraySort
{
    class Program
    {
        static void Main(string[] args)
        {
            var colours = new string[5] { "red", "blue", "orange", "white", "black" };
            Array.Sort(colours);
            var join = string.Join(", ", colours);
            Console.WriteLine(join);
        }
    }
}
